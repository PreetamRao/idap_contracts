//..............................................
//.IIIII.DDDDDDDDD.......AAAAA.....PPPPPPPPP....
//.IIIII.DDDDDDDDDD......AAAAA.....PPPPPPPPPP...
//.IIIII.DDDDDDDDDDD.....AAAAAA....PPPPPPPPPPP..
//.IIIII.DDD....DDDDD...AAAAAAA....PPPP...PPPP..
//.IIIII.DDDD....DDDD...AAAAAAA....PPPP...PPPP..
//.IIIII.DDDD....DDDD..AAAAAAAAA...PPPPPPPPPPP..
//.IIIII.DDDD....DDDD..AAAA.AAAA...PPPPPPPPPPP..
//.IIIII.DDDD....DDDD..AAAAAAAAAA..PPPPPPPPPP...
//.IIIII.DDDD....DDDD.AAAAAAAAAAA..PPPPPPPPP....
//.IIIII.DDD....DDDD..AAAAAAAAAAA..PPPP.........
//.IIIII.DDDDDDDDDDD..AAAAAAAAAAAA.PPPP.........
//.IIIII.DDDDDDDDDD..AAAA.....AAAA.PPPP.........
//.IIIII.DDDDDDDDD...AAAA.....AAAA.PPPP.........
//...............https://www.idap.io............

pragma solidity 0.4.21;
import "./base/SafeMath.sol";
import "./base/Ownable.sol";
import "./oracle/EthPriceTicker.sol";


contract IdapToken {
    function transfer (address, uint) public pure { }
    function burn (uint256) public pure { }
    function finalize() public pure { }
    function transferOwnership (address) public pure { }
}


contract CrowdSale is EthPriceTicker, Ownable {
    using SafeMath for uint256;

// The token being sold
    IdapToken public tokenReward;
//Begin: state variables

    enum State { fundraising, success, failed, paused, closed }
    State public state = State.fundraising;
    string public version = "1.0";
    uint public hardCap = 66685 ether;   // maximum funds in wei to be raised
    uint public totalRaisedInWei; //total funds raises in wei
    //uint public rateIco = 1170;  // number of tokens per wei in ico stage
    uint rateIco = 60;  // Number of IDap at 0.6 USD price = 60 cents 
    uint public fundingStartBlock;  //start block of funding
    uint public firstChangeBlock;
    uint public secondChangeBlock;
    uint public thirdChangeBlock;
    uint public fundingEndBlock;
    uint public minimumPricePreIcoInWei = 0.5 ether; // minimum price for pre ico investers in ether
    //uint public minimumPriceIcoInWei; // minimum price for normal ico investers in ether
    uint public tokensDistributed = 0; // Number of tokens distributed

//End: state variables
//Begin: modifiers

    modifier inState(State _state) {
        require(_state == state);
        _;
    }

    modifier isMinimumPrice() {
        if (block.number < thirdChangeBlock && block.number >= fundingStartBlock) {
            require(msg.value >= minimumPricePreIcoInWei);
        } else if (block.number >= thirdChangeBlock && block.number <= fundingEndBlock) {
            require(msg.value >= 0.01 ether);
        }
        _;
    }

    modifier isIcoOpen() {
        require(block.number >= fundingStartBlock);
        require(block.number <= fundingEndBlock);
        require(totalRaisedInWei <= hardCap);
        _;
    }

    modifier isEndOfLifeCycle() {
        require((state == State.closed || state == State.failed));
        _;
    }

    modifier isIcoFinished() {
        require(totalRaisedInWei >= hardCap || block.number > fundingEndBlock || state == State.success);
        _;
    }

//End: modifiers

//Begin: constructor

    /**
    * CONSTRUCTOR
    * @param _fundingStartBlock funding start block, E.g 5531951
    * @param _firstChangeBlock funding first blockchange , thats ends preiCo stage
    * @param _secondChangeBlock funding second blockchange , thats ends main sale 1st tranhce stage
    * @param _thirdChangeBlock funding third blockchange , thats ends main sale 2nd tranhce stage
    * @param _durationInHours funding duration in hours , thats ends main sale 3nd tranhce stage
    * @param _averageBlockTime average block time , we are calcalute end block using this and duration in hours
    * @param _initialEthUsdCentsPrice uint256  Price of 1ETH in terms of USD cents.
    *        E.g. if 1ETH=650.25USD then initialise this as 65025.
    * @param _tokenAddress address  The address of the deployed Idap token
    * @param _fundWallet address  The address of Idap fund sink to which
    *        the contributed ETH is forwarded.
    */

    function CrowdSale(
        uint _fundingStartBlock,
        uint _firstChangeBlock,
        uint _secondChangeBlock,
        uint _thirdChangeBlock,
        uint _durationInHours,
        uint _averageBlockTime,
        uint _initialEthUsdCentsPrice,
        IdapToken _tokenAddress,
        address _fundWallet
    ) public Ownable(_fundWallet) {

        require(_fundingStartBlock > 0);
        require(_durationInHours > 0);
        require(_firstChangeBlock > _fundingStartBlock && _firstChangeBlock < _secondChangeBlock);
        require(_secondChangeBlock < _thirdChangeBlock);
        require(_averageBlockTime > 0);
        require(_tokenAddress != address(0));
        //require(_initialEthUsdCentsPrice >= 65000);
        creater = msg.sender;
        fundingStartBlock = _fundingStartBlock;
        fundingEndBlock = _fundingStartBlock.add(_durationInHours.mul((3600 / _averageBlockTime)));
        firstChangeBlock = _firstChangeBlock;
        secondChangeBlock = _secondChangeBlock;
        thirdChangeBlock = _thirdChangeBlock;
        EtherPriceInCent = _initialEthUsdCentsPrice;
        tokenReward = IdapToken(_tokenAddress);
    }


//End: constructor

    /**
     * allocating tokens to beneficiary address
     *
     *  1) 0.5 USD = 1 IDAP, so we first convert the sent wei into USD
     *  2) Add Etheres received in total raised ethers in ICO
     *  3) Increase number of  distrubuted tokens
     *  4) Forward the sent wei to funds wallet address
     */

    function buyTokens(address _beneficiary)  public inState(State.fundraising) isMinimumPrice isIcoOpen payable {
        require(_beneficiary != 0x0);
        bool  lockTransfer = false;
        uint rate = getCurrentRate(getUsdCentValue(msg.value));
        uint tokenAmount;
        uint receivedWei = totalRaisedInWei.add(msg.value);

        if (receivedWei > hardCap) {
            lockTransfer = true;
            totalRaisedInWei = totalRaisedInWei.add((hardCap.sub(totalRaisedInWei)));
            // // Calculate how many tokens (in units of Wei) should be awarded on this transaction
            //tokenAmount = rate.mul((hardCap.sub(totalRaisedInWei)));
            tokenAmount = getCurrentRate(getUsdCentValue(hardCap.sub(totalRaisedInWei)));
            tokensDistributed = tokensDistributed.add(tokenAmount);
            // Send change extra ether to user
        } else {
            totalRaisedInWei = totalRaisedInWei.add(msg.value);
            //tokenAmount = rate.mul(msg.value);
            tokenAmount = rate;
            tokensDistributed = tokensDistributed.add(tokenAmount);
        }

        /* contributors[_beneficiary].contriAddress = _beneficiary;
        contributors[_beneficiary].amount = msg.value;
        contributors[_beneficiary].tokens = tokenAmount; */
        if (lockTransfer) {
            _beneficiary.transfer(receivedWei.sub(hardCap));
        }
        tokenReward.transfer(_beneficiary, tokenAmount);
        // immediately transfer ether to fundsWallet
        sendFundsToOwner();

    }

    function changeIdapTokenOwner(address _newOwner) external  onlyOwner  inState(State.paused){ // solhint-disable-line
        require(_newOwner != address(0));
        tokenReward.transferOwnership(_newOwner);
    }

    /**
     * Returns the equivalent value in USD cents for the value of wei sent.
     *
     * E.g. if 1 ETH = 685.25 USD which is 68,525 cents (30th April 2018) then 0.1 ETH which is
     * 100000000000000000 wei will be worth 68.52 USD or 6852 cents.
     *
     * NOTE: there's no rounding!
     */
    function getUsdCentValue(uint256 _wei) private view returns (uint256) {
        return (_wei * EtherPriceInCent) / 1 ether;
    }

  // after ICO only owner can call this
    function burnRemainingToken(uint256 _value) external  onlyOwner isIcoFinished { // solhint-disable-line
        //@TODO - check balance of address if no value passed
        require(_value > 0);
        tokenReward.burn(_value);
    }

    // only owner can call this , if we have to upgrade crowdsale than we can transfer
    function withdrawRemainingToken(uint256 _value, address tokenAdmin) external onlyOwner inState(State.paused) { // solhint-disable-line
        //@TODO - check balance of address if no value passed
        require(tokenAdmin != 0x0);
        require(_value > 0);
        tokenReward.transfer(tokenAdmin, _value);
    }

    function destroyContract() external onlyOwner isEndOfLifeCycle {
        selfdestruct(msg.sender);
    }

    /**
    * Incoming transfer: the fallback function
    *
    * When someone just sends Ether to the contract address without
    * invoking the buyTokens() function, we trigger the buyTokens
    * function.
    * NOTE: as this is a standard transaction the buyTokens function
    * should not exceed the std. tx gas limit.
    */
    function() external payable {
        buyTokens(msg.sender);
    }

 /// @dev Pauses the contract
    function pause() external onlyOwner inState(State.fundraising) {
        // Move the contract to Paused state
        state = State.paused;
    }

    /// @dev Resume the contract
    function resume() external onlyOwner  inState(State.paused) {
        // Move the contract out of the Paused state
        state = State.fundraising;
    }

    function sendFundsToOwner() private {
        owner.transfer(msg.value);
    }

    // ethers we transferred for oracalize ,we can withdraw  if anything leftafer ico ends
    function withdrawFunds() external isIcoFinished {
        owner.transfer(address(this).balance);
    }

    // making it public to use in adim dashbaord and user dashboard
    function getCurrentRate(uint256 _centValue)  public constant returns (uint) {
        //@TODO: if we have to multiply here by 10*18
        uint tokens = _centValue.mul(10**18).div(rateIco);
        if (block.number < firstChangeBlock && block.number > fundingStartBlock) {
            return (tokens.add((tokens.mul(30)/100)));
        } else if (block.number < secondChangeBlock && block.number > firstChangeBlock) {
            return (tokens.add((tokens.mul(20)/100)));
        } else if (block.number < thirdChangeBlock && block.number > secondChangeBlock) {
            return (tokens.add((tokens.mul(10)/100)));
        } else if (block.number < fundingEndBlock && block.number > thirdChangeBlock) {
            return tokens;
        }
    }

}
