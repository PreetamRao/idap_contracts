//..............................................
//.IIIII.DDDDDDDDD.......AAAAA.....PPPPPPPPP....
//.IIIII.DDDDDDDDDD......AAAAA.....PPPPPPPPPP...
//.IIIII.DDDDDDDDDDD.....AAAAAA....PPPPPPPPPPP..
//.IIIII.DDD....DDDDD...AAAAAAA....PPPP...PPPP..
//.IIIII.DDDD....DDDD...AAAAAAA....PPPP...PPPP..
//.IIIII.DDDD....DDDD..AAAAAAAAA...PPPPPPPPPPP..
//.IIIII.DDDD....DDDD..AAAA.AAAA...PPPPPPPPPPP..
//.IIIII.DDDD....DDDD..AAAAAAAAAA..PPPPPPPPPP...
//.IIIII.DDDD....DDDD.AAAAAAAAAAA..PPPPPPPPP....
//.IIIII.DDD....DDDD..AAAAAAAAAAA..PPPP.........
//.IIIII.DDDDDDDDDDD..AAAAAAAAAAAA.PPPP.........
//.IIIII.DDDDDDDDDD..AAAA.....AAAA.PPPP.........
//.IIIII.DDDDDDDDD...AAAA.....AAAA.PPPP.........
//...............https://www.idap.io............

/*
   Bitcoinaverage-based ETH/USD price ticker
   This contract keeps in storage an updated ETH/USD price,
   which is updated every ~3600 seconds(1 hour).
   https://www.idap.io
*/

pragma solidity 0.4.21;
//import "./oraclizeAPI.sol";
import "github.com/oraclize/ethereum-api/oraclizeAPI.sol";


/**
 * Contract which exposes `EtherPriceInCent` which is the Ether price in USD cents.
 * E.g. if 1 Ether is sold at 840.32 USD on the markets, the `EtherPriceInCent` will
 * be `84032`.
 *
 * This price is supplied by Oraclize callback, which sets the value. Currently
 * there is no proof provided for the callback, other then the value and the
 * corresponding ID which was generated when this contract called Oraclize.
 *
 * If this contract runs out of Ether, the callback cycle will interrupt until
 * the `update` function is called with a transaction which also replenishes the
 * balance of the contract.
 */


contract EthPriceTicker is usingOraclize {


// storage starts
    uint256 public EtherPriceInCent;
    mapping(bytes32=>bool) public validIds;
     // Allow only single auto update cycle, to prevent DOS attack.
    bytes32 private autoUpdateId;
// storage ends


// events starts
    event NewOraclizeQuery(string description);
    event PriceLog(string price);
// events ends    


    function EthPriceTicker() public {
        //oraclize_setProof(proofType_TLSNotary | proofStorage_IPFS);
        /*It is very error prone to test oraclize in truffle or any local test net which effects the testing of other contracts as well
        uncomment below two lines of code to test in truffle and see other details in read me or uncomment only updatePrice() function
        to test in rinkeby or ropsten on remix */

        //OAR = OraclizeAddrResolverI(0x92459e216d5f0bc2a22fa4916d9bec281ceee9d4);
        //uncomment only updatePrice() to test in remix.
        //updatePrice();
    }

    function updatePrice() public payable {
        require(oraclize_getPrice("URL") <= address(this).balance);
        require(autoUpdateId == bytes32(0));
        emit NewOraclizeQuery("Oraclize query was sent, Requesting Oraclize to submit latest ETHUSD price"); // solhint-disable-line
        // uint callbackGas = 200000; // amount of gas we want Oraclize to set for the callback function
        // bytes32 queryId = oraclize_newRandomDSQuery(delay, N, callbackGas); // this function internally generates the correct oraclize_query and returns its queryId
        bytes32 queryId = oraclize_query(60, "URL", "json(https://apiv2.bitcoinaverage.com/convert/global?from=ETH&to=USD).price");
        validIds[queryId] = true;
        autoUpdateId = queryId;
    
    }

    function instantUpdatePrice() public payable {
        require(oraclize_getPrice("URL") <= msg.value);
        emit NewOraclizeQuery("Oraclize query was sent,Requesting Oraclize to submit latest ETHUSD price instantly"); // solhint-disable-line
        bytes32 qId = oraclize_query("URL", "json(https://apiv2.bitcoinaverage.com/convert/global?from=ETH&to=USD).price");
        validIds[qId] = true;
    }

    function __callback(bytes32 _cbid, string _result) public {
        require(msg.sender == oraclize_cbAddress());
        require(validIds[_cbid]);
        
        //@TO DO: value should not be less than
        EtherPriceInCent = parseInt(_result, 2);
        delete validIds[_cbid];

        emit PriceLog(_result); // solhint-disable-line

        if (_cbid == autoUpdateId) {
            autoUpdateId = bytes32(0);
        } else {
            // Don't auto update if it was instant update.
            return;
        }
        // Exit to not revert received price.
        require(oraclize_getPrice("URL") <= address(this).balance);
        updatePrice();
    }

}
