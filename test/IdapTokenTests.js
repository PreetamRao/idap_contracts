const idap = artifacts.require('./IdapToken.sol');

contract(idap,(accounts) => {

  const TokenOwnerAddress = accounts[0];
  const initialSupply = 10000000000000000000000;
  const beneficiaryAddress = accounts[1];
  const startBlock = 1; //block number 1
  const duration = 10 ;

  const cliffBlock =  10;  //after 10 blocks will be mined
  const role = 'team';
  //block numbers must be adjusted to run tests on other machines
  //to check the contract is initialised with the values passed in the constructor;

  it('should be initialized with the constuctor values',() => {
    let app;
    return idap.deployed().then((instance) => {
      app =  instance;
      return app.owner.call();
    }).then((data) => {
      assert.equal(data,TokenOwnerAddress,'token owner address must be equal to passed address');
    })
      .then(() => {
        return app.totalSupply.call();
      }).then((data) => {
        assert.equal(data.toNumber(),initialSupply,'Total supply must be equal to passed initial supply in the constructor');
      });
  });
  //this test will fail in solidity coverage because grantToBeneficiary is called from solidity coverage test rpc port which contains different owner address
  it('should be showing correct number of vested tokens at a given timestamp',() => {
    let app;
    return idap.deployed().then((instance) => {
      app =  instance;
      return app.grantToBeneficiary(500,beneficiaryAddress,true,startBlock,cliffBlock,duration,15,role);
      155294118000000000000000000,"0x1DaC5Dc3C996E768b454d2c8202bA3646e1976Fb",true,2091171,
    }).then(() => {

      return app.getVestingTokensDetail(beneficiaryAddress);

    }).then((data)=>{
      assert.equal(data[2].c,500,`granted tokens must be equal to ${500}` );
      console.log(data);
      return app.getVestingSchedule(beneficiaryAddress);

    }).then((data)=>{
      console.log(data);

    });

  });

});
